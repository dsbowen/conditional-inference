from itertools import product

from multiple_inference.bayes import Improper


def run_common_methods(results):
    # test that you can run all the common methods without error
    results.conf_int()
    for simultaneous in (True, False):
        results.rank_conf_int(simultaneous=simultaneous)
        results.rank_point_plot(simultaneous=simultaneous)
    for superset, criterion in product((True, False), ("fwer", "fdr")):
        results.compute_best_params(superset=superset, criterion=criterion)
    results.expected_wasserstein_distance()
    results.likelihood()
    if not isinstance(results.model, Improper):
        results.line_plot(0)
    results.point_plot()
    results.rank_matrix_plot()
    results.reconstruction_point_plot()
    results.summary()
