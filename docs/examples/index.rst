Examples
========

.. toctree::
    :maxdepth: 1
    
    multiple_inference.ipynb
    bayes_primer.ipynb
    rank_conditions.ipynb
    stats.ipynb