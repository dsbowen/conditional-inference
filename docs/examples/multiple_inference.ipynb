{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Multiple Inference Cookbook\n",
    "\n",
    "This template is an 80-20 solution for multiple inference. It uses many of the latest techniques in multiple inference to answer the following questions:\n",
    "\n",
    "1. *Compare to zero.* Which parameters are significantly different from zero?\n",
    "2. *Compare to the average.* Which parameters are significantly different from the average (i.e., the average value across all parameters)?\n",
    "3. *Pairwise comparisons.* Which parameters are significantly different from which other parameters?\n",
    "4. *Distribution.* What does the distribution of parameters look like?\n",
    "5. *Ranking.* What is the ranking of each parameter?\n",
    "6. *Selection.* Which parameters might be the largest (i.e., the highest ranked)?\n",
    "7. *Inference after ranking.* What are the values of the parameters given their rank? e.g., What is the value of the parameter with the largest estimated value?\n",
    "\n",
    "Click the badge below to use this template on your own data. This will open the notebook in a Jupyter binder.\n",
    "\n",
    "[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/dsbowen%2Fconditional-inference/HEAD?urlpath=lab/tree/docs/examples/multiple_inference.ipynb)\n",
    "\n",
    "Instructions:\n",
    "\n",
    "1. Upload a file named `data.csv` to this folder with your conventional estimates. Open `data.csv` to see an example. In this file, we named our dependent variable \"dep_variable\", and have estimated parameters named \"policy0\",..., \"policy9\". The first column of `data.csv` contains the conventional estimates $m$ of the unknown parameters. The remaining columns contain consistent estimates of the covariance matrix $\\Sigma$. In `data.csv`, $m=(0, 1,..., 9)$ and $\\Sigma = I$.\n",
    "2. Modify the code if necessary.\n",
    "3. Run the notebook.\n",
    "\n",
    "### Runtime warnings and long running times\n",
    "\n",
    "If you are estimating many parameters or the parameters effects are close together, you may see `RuntimeWarning` messages and experience long runtimes. Runtime warnings are common, usually benign, and can be safely ignored."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "import seaborn as sns\n",
    "\n",
    "from multiple_inference.bayes import Improper, Nonparametric, Normal\n",
    "from multiple_inference.confidence_set import ConfidenceSet, AverageComparison, PairwiseComparison, SimultaneousRanking\n",
    "from multiple_inference.rank_condition import RankCondition\n",
    "\n",
    "data_file = \"data.csv\"\n",
    "alpha = .05\n",
    "\n",
    "np.random.seed(0)\n",
    "sns.set()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, we'll summarize and plot your original (conventional) estimates."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = Improper.from_csv(data_file, sort=True)\n",
    "results = model.fit(title=\"Conventional estimates\")\n",
    "results.summary(alpha=alpha)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results.point_plot(alpha=alpha)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we'll create a *reconstruction plot*. A reconstruction plot shows what we would expect the conventional estimates to look like if your estimates were correct.\n",
    "\n",
    "For example, imagine we ran a randomized control trial in which we tested the effects of ten treatments. We then obtained our conventional estimates using OLS and rank ordered them. Now, suppose our conventional estimates are correct (i.e., assume the true effect of each treatment follows its OLS distribution). What would our new estimates look like if we reran the experiment, estimated the treatment effects by OLS, and rank ordered them again?\n",
    "\n",
    "Ideally, the new conventional estimates would look like our original conventional estimates. Below, the blue dots show the simulated distribution of new estimates. The orange x's show our original conventional estimates. So, ideally, the blue dots should be on top of the orange x's.\n",
    "\n",
    "Conventional estimates are more spread out than the true parameters in expectation. That is, conventional estimates often suggest that some parameters are very different from others, even if they are all similar. We call this problem *fictitious variation*. Our conventional estimates suffer from fictitious variation when the blue dots are more spread out than the orange x's."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results.reconstruction_point_plot(title=\"Conventional estimates reconstruction plot\")\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compare to zero\n",
    "\n",
    "First, we'll test which parameters significantly differ from zero. There are at least two criteria we could apply in answering this question. First, we might want to control the *false discovery rate*. The false discovery rate is the proportion of rejected hypotheses that are truly null. This testing procedure begins by converting p-values into *q-values*. We start by splitting the distribution of p-values into two components: a uniform distribution and a right-skewed distribution. (If a null hypothesis is true, its p-value will follow a uniform distribution. If a null hypothesis is false, its p-value will follow a right-skewed distribution.) Using these components, we can estimate the proportion of true nulls we would reject (the q-value) by rejecting all hypotheses with a p-value below a given significance threshold. Look at the \"qvalue\" column in the table below. Suppose we reject all null hypotheses with a q-value below 0.05. In that case, about 5\\% of the rejected hypotheses are truly null.\n",
    "\n",
    "Second, we might want to control the *family-wise error rate*. The family-wise error rate is the probability of making at least one false discovery. That is, it is the probability of rejecting at least true null hypothesis. We can control the family-wise error rate using *simultaneous confidence intervals*. Suppose we estimated $K$ parameters. Simultaneous confidence intervals begin by forming a $K$-dimensional rectangle such that all the parameters fall within this rectangle with 95\\% probability. Then, they \"project\" this $K$-dimensional rectangle onto the dimension for a specific parameter to obtain that parameter's simultaneous confidence interval. By construction, all the parameters fall within their simultaneous confidence interval with 95\\% probability. Look at the \"pvalue\" column in the table below. Suppose we reject all null hypotheses with a p-value below 0.05. In that case, there is a 5\\% chance that any rejected hypotheses are truly null. Simultaneous confidence intervals are similar to a Bonferroni correction but have higher power. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = ConfidenceSet.from_csv(data_file, sort=True)\n",
    "results = model.fit()\n",
    "results.summary(alpha=alpha)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = results.point_plot(alpha=alpha)\n",
    "ax.axvline(0, linestyle=\"--\")\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also use a stepdown improvement to increase power while controlling the family-wise error rate. This is analogous to Holm's stepdown improvement on the Bonferroni correction."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results.test_hypotheses(alpha=alpha)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compare to the average\n",
    "\n",
    "Next, we'll use q-values and simultaneous confidence intervals to discover which parameters significantly differ from the average (i.e., the average across all parameters). This is similar to what we did before. However, instead of constructing q-values and simultaneous confidence intervals for the parameters $\\mu_k$, we'll construct them for the difference between the parameters and the average $\\mu_k - \\mathbf{E}[\\mu_j]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = AverageComparison.from_csv(data_file, sort=True)\n",
    "results = model.fit()\n",
    "results.summary(alpha=alpha)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The x-axis in the plot below is the estimated difference between each parameter and the average $\\mu_k - \\mathbf{E}[\\mu_j]$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ax = results.point_plot(alpha=alpha)\n",
    "ax.axvline(0, linestyle=\"--\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results.test_hypotheses(alpha=alpha)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Pairwise comparisons\n",
    "\n",
    "Next, we'll use q-values and simultaneous confidence intervals to perform pairwise comparisons. Instead of constructing q-values and simultaneous confidence intervals for the individual parameters $\\mu_k$, we'll construct them for all pairwise differences $\\mu_j - \\mu_k \\quad \\forall j, k$.\n",
    "\n",
    "First, we'll perform hypothesis testing using q-values by setting `criterion=\"fdr\"` for \"false discovery rate.\" Green squares in the heatmap below indicate that we have rejected the null hypothesis $H_{j,k}: \\mu_k = \\mu_j$, where $j$ is the parameter on the x-axis and $k$ is the parameter on the y-axis. Red squares indicate that we have not rejected the null hypothesis. Of the rejected hypotheses, we expect that at most $\\alpha$ proportion are truly null."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = PairwiseComparison.from_csv(data_file, sort=True)\n",
    "results = model.fit()\n",
    "results.hypothesis_heatmap(alpha=alpha, triangular=True, criterion=\"fdr\")\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Second, we'll perform hypothesis testing using simultaneous confidence intervals by setting `criterion=\"fwer\"` for \"family-wise error rate.\" Green squares in the heatmap below indicate that we have rejected the null hypothesis $H_{j,k}: \\mu_j \\leq \\mu_k$. That is, we have concluded that the parameter on the x-axis is greater than the parameter on the y-axis. Red squares indicate that we have not rejected the null hypothesis. We expect that we have rejected at least one truly will hypothesis with probability $\\alpha$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results.hypothesis_heatmap(alpha=alpha, triangular=True, criterion=\"fwer\")\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Distributions\n",
    "\n",
    "Next, we'll use Bayesian estimators to understand the distribution of parameters. Bayesian estimators begin with a *prior* distribution and then update that belief based on data using Bayes' Theorem to obtain a *posterior* distribution.\n",
    "\n",
    "Classical Bayesian estimators take the prior as given. However, we can often obtain better estimates by using empirical Bayes to estimate the prior from the data. For example, imagine predicting MLB players' on-base percentage (OBP) next season. We might predict that a player's OBP next season will be the same as his OBP in the previous season. But how can we predict the OBP for a rookie with no batting history? One solution is to predict that the rookie's OBP will be similar to last season's rookies' OBP. In Bayesian terms, we've constructed a prior belief about *next* season's rookies' OBP using data from the *previous* season's rookies' rookies' OBP.\n",
    "\n",
    "Empirical Bayes uses the same logic. Imagine randomly selecting one parameter $\\mu_k$ and putting the data we used to estimate it in a locked box. We can use the data about the remaining parameters to estimate a prior distribution for $\\mu_k$.\n",
    "\n",
    "Empirical Bayes estimators can be parametric or non-parametric. Parametric empirical Bayes assumes the shape of the prior distribution. Nonparametric empirical Bayes does not assume the shape of the prior distribution. Below, we apply a parametric empirical Bayes estimator assuming a normal prior distribution.\n",
    "\n",
    "First, let's plot one parameter's prior, conventional, and posterior distributions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = Normal.from_csv(data_file, sort=True)\n",
    "parametric_results = model.fit()\n",
    "parametric_results.line_plot(0)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, let's create a reconstruction plot for the Bayesian estimator. This plot shows what we would expect the conventional estimates to look like if the Bayesian model were correct. Remember that we want the blue dots to be on top of the orange x's.\n",
    "\n",
    "Compare the reconstruction plot for the Bayesian estimates (below) to the reconstruction plot for the conventional estimates near the top of the notebook. Looking at the reconstruction plot for the conventional estimates at the top of the notebook, you'll likely see that the conventional estimates are too spread out (the blue dots are more spread out than the orange x's). Looking at the reconstruction plot for the Bayesian estimates below, you'll likely see that the Bayesian estimates are appropriately spread out (the blue dots are on top of the orange x's)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parametric_results.reconstruction_point_plot(title=\"Parametric empirical Bayes reconstruction plot\")\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If the reconstruction plot looks good, does that mean our parametric assumption (that the prior is a normal distribution) is correct? What if the assumption is wrong?\n",
    "\n",
    "Amazingly, parametric empirical Bayes estimators usually give us more accurate point estimates than conventional estimators regardless of the prior distribution. Some, like the James-Stein estimator, are mathematically guaranteed to do so. Additionally, we can estimate \"robust\" confidence intervals that have correct coverage regardless of the prior distribution.\n",
    "\n",
    "Let's summarize and plot the Bayesian estimates using robust confidence intervals."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parametric_results.summary(alpha=alpha, robust=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parametric_results.point_plot(alpha=alpha, robust=True)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's repeat this exercise with a nonparametric empirical Bayes estimator. Remember, parametric empirical Bayes estimators assume the shape of the prior distribution (e.g., normal). Nonparametric empirical Bayes estimators do not assume the shape of the prior distribution. We'll start by plotting the prior, conventional estimate, and posterior for a single parameter."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = Nonparametric.from_csv(data_file, sort=True)\n",
    "nonparametric_results = model.fit()\n",
    "nonparametric_results.line_plot(0)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nonparametric_results.reconstruction_point_plot(title=\"Nonparametric empirical Bayes reconstruction plot\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nonparametric_results.summary(alpha=alpha)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nonparametric_results.point_plot(alpha=alpha)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Should you use a parametric or nonparametric empirical Bayes estimator? In my experience, parametric empirical Bayes is better for a small number of parameters, and nonparametric empirical Bayes is better for a large number of parameters. If both estimators give similar point estimates, trust the one with wider confidence intervals (usually the parametric version). If the estimators give very different point estimates, my rule of thumb is to trust parametric empirical Bayes when estimating fewer than 50 parameters and nonparametric empirical Bayes otherwise.\n",
    "\n",
    "This notebook is an 80-20 solution for multiple inference. For a 90-50 solution, use cross-validation to decide between parametric and nonparametric empirical Bayes."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Ranking\n",
    "\n",
    "Next, we'll estimate each parameter's rank. For example, we might say that we're 95% confident that the parameter with the largest estimated value is genuinely one of the largest three parameters. One way to do this is by sampling from a Bayesian estimator's posterior distribution and seeing how often parameter $k$ falls in a certain rank."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parametric_results.rank_point_plot(alpha=alpha)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nonparametric_results.rank_point_plot(alpha=alpha)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Selection\n",
    "\n",
    "Selection is closely related to ranking. We often want to select a set of parameters that we're confident rank near the top.\n",
    "\n",
    "For example, we often want to know parameters might be the largest (i.e., highest-ranked). That is, we want a set of parameters containing the best (largest) parameter with 95\\% confidence. We can use Bayesian estimators to construct this set. Starting from an empty set, we continue adding parameters until we're confident that one of them is genuinely the best according to the posterior distribution.\n",
    "\n",
    "Parameters with \"True\" next to them in the table below are in this set. Parameters with \"False\" next to them are not."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "parametric_results.compute_best_params(alpha=alpha, criterion=\"fwer\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "nonparametric_results.compute_best_params(alpha=alpha, criterion=\"fwer\")"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inference after ranking\n",
    "\n",
    "Finally, we'll estimate parameters given the rank of their conventional estimates. For example, imagine we ran a randomized control trial testing ten treatments and want to estimate the effectiveness of the top-performing treatment.\n",
    "\n",
    "One property we want our estimators to have is *quantile-unbiasedness*. An estimator is quantile-unbiased if the true parameter falls below its $\\alpha$-quantile estimate with probability $\\alpha$ given its estimated rank. For example, the true effect of the top-performing treatment should fall below its median estimate half the time.\n",
    "\n",
    "Similarly, we want confidence intervals to have *correct conditional coverage*. Correct conditional coverage means that the parameter should fall within our $\\alpha$-level confidence interval with probability $1-\\alpha$ given its estimated rank. For example, the true effect of the top-performing treatment should fall within its 95% confidence interval 95% of the time.\n",
    "\n",
    "Below, we compute the optimal quantile-unbiased estimates and conditionally correct confidence intervals for each parameter given its rank."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = RankCondition.from_csv(data_file, sort=True)\n",
    "results = model.fit(title=\"Conditional estimates\")\n",
    "results.summary(alpha=alpha)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results.point_plot(alpha=alpha)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Conditional inference is a strict requirement. Conditionally quantile-unbiased estimates can be highly variable. And conditionally correct confidence intervals can be unrealistically long. We can often obtain more reasonable estimates by focusing on *unconditional* inference instead of *conditional* inference.\n",
    "\n",
    "Imagine we ran our randomized control trial 10,000 times and want to estimate the effect of the top-performing treatment. We need *conditional* inference if we're interested the subset of trials where a specific parameter $k$ was the top performer. However, we can use *unconditional* inference if we're only interested in being right \"on average\" across all 10,000 trials.\n",
    "\n",
    "Below, we use *hybrid estimates* to compute approximately quantile-unbiased estimates and unconditionally correct confidence intervals for each parameter.\n",
    "\n",
    "If you don't know whether you need conditional or unconditional inference, use unconditional inference."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results = model.fit(beta=.005, title=\"Hybrid estimates\")\n",
    "results.summary(alpha=alpha)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "results.point_plot(alpha=alpha)\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Both Bayesian and rank condition estimators give parameter estimates and confidence intervals. Which ones should we use?\n",
    "\n",
    "We should use Bayesian estimators if our goal is to understand the distribution of parameters. If, however, our goal is to estimate a parameter given its rank (e.g., the top-performing parameter), the answer is less clear. In my experience, Bayesian point estimates are better than rank condition point estimates, but rank condition confidence intervals are better than Bayesian confidence intervals. For example, if you want to estimate the top-performing parameter, I recommend writing your discussion section using the Bayesian point estimate and the rank condition (hybrid) confidence interval.\n",
    "\n",
    "Again, this template is an 80-20 solution for multiple inference. For a 90-50 solution, use cross-validation to decide whether Bayesian or rank condition estimates are better for your data set."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.9.0 ('conditional-inference')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.11"
  },
  "vscode": {
   "interpreter": {
    "hash": "a31fe93114e6fe9c0b874076e62df141d5b35f609e1bfa94ca168a298e55e549"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
