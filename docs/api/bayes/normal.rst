multiple\_inference.bayes.normal
===================================

.. automodule:: multiple_inference.bayes.normal
   :members:
   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      Normal
      NormalResults

   .. rubric:: Functions

   .. autosummary::
   
      compute_robust_critical_value
   
   

   
   
   



