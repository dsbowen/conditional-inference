multiple\_inference.stats
============================

.. automodule:: multiple_inference.stats
   :members:
   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      joint_distribution
      mixture
      quantile_unbiased
      truncnorm
      
   
   

   
   
   



