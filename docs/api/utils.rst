multiple\_inference.utils
============================

.. automodule:: multiple_inference.utils
   :members:
   
   
   

   
   
   .. rubric:: Functions

   .. autosummary::
   
      expected_wasserstein_distance
      holm_bonferroni_correction
      weighted_quantile
   
   

   
   
   

   
   
   



