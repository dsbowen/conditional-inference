multiple\_inference.confidence_set
=====================================

.. automodule:: multiple_inference.confidence_set
   :members:
   
   
   

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
        ConfidenceSetResults
        ConfidenceSet
        AverageComparison
        BaselineComparison
        PairwiseComparisonResults
        PairwiseComparison
        MarginalRankingResults
        MarginalRanking
        SimultaneousRankingResults
        SimultaneousRanking
   

   
   
   



